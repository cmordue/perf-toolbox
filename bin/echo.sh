#!/bin/sh

cmd=$1
DIR="$( cd "$( dirname "$0" )" && pwd )"
target=${DIR}/../target
piddir=${DIR}/pids
pidfile=${piddir}/echo.pid
mainclass=org.adroitlogic.toolbox.ToolBoxApp

start() {
	echo "starting echo server"
	mkdir -p ${piddir}
	java -server -Xms512M -Xmx512M \
	  -Dorg.apache.xerces.xni.parser.XMLParserConfiguration=org.apache.xerces.parsers.XMLGrammarCachingConfiguration \
	  -cp `cat ${target}/.classpath && echo ":${target}/soa-toolbox-1.8.0.jar"` \
	  ${mainclass} -echo &
	echo $! > ${pidfile}	
}

stop() {
	if [ -s ${pidfile} ]; then
		pid=`cat ${pidfile}`
		echo "Stopping echo server with pid: ${pid}"
		kill ${pid}
		rm ${pidfile}
	else
		pid="`ps aux | grep \"${mainclass}\" | grep -v grep | awk '{print $2}'`"
		if [ ${pid} > 0 ]; then 
			echo "Killing echo server with pid: ${pid}"
			kill ${pid}
		else
			echo "failed to stop echo server: no pid file nor process could be found"
		fi
	fi	
}

case "${cmd}" in
	start )
		start
		;;
	stop )
		stop
		;;
esac

