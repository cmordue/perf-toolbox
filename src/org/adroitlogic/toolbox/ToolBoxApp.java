/*
 * ToolBoxApp.java
 */
package org.adroitlogic.toolbox;

import org.adroitlogic.toolbox.tabs.EchoServer;
import org.adroitlogic.toolbox.tabs.HttpClientWrapper;
import org.adroitlogic.toolbox.tabs.JettyWrapper;
import org.apache.log4j.PropertyConfigurator;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class ToolBoxApp extends SingleFrameApplication {

    private HttpClientWrapper httpClient = new HttpClientWrapper();

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        show(new ToolBoxView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of ToolBoxApp
     */
    public static ToolBoxApp getApplication() {
        return Application.getInstance(ToolBoxApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        PropertyConfigurator.configure(ClassLoader.getSystemClassLoader().
                getResource("org/adroitlogic/toolbox/resources/log4j.properties"));

        if (args.length == 1 && "-jetty".equals(args[0])) {
            JettyWrapper jetty = new JettyWrapper();
            jetty.start(9000);
            System.out.println("Started sample Jetty server on port 9000...");
        } else if (args.length == 1 && "-echo".equals(args[0])) {
            EchoServer es = new EchoServer();
            System.out.println("Starting sample Echo server on port 9000...");
            es.run(9000);
        } else if (args.length == 2 && "-echo".equals(args[0]) && isInteger(args[1])) {
            EchoServer es = new EchoServer();
            int port = Integer.parseInt(args[1]);
            System.out.println("Starting sample Echo server on port " + port + "...");
            es.run(port);
        } else {
            launch(ToolBoxApp.class, args);
        }
    }

    public static boolean isInteger(String arg) {
        try {
            Integer.parseInt(arg);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
