/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adroitlogic.toolbox.tabs;

import java.awt.TextArea;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author asankha
 */
public class Relay extends Thread {

    private final byte buf[] = new byte[1024];
    private final InputStream in;
    private final OutputStream out;
    private final OutputStream os;
    private final Socket socket;
    private final boolean hex;
    private final StringBuilder sb = new StringBuilder(16 * 1024);
    private final JTextArea textArea;
    private FileOutputStream fos;
    private String enc = "8859_1";
    private volatile boolean exit;

    public Relay(InputStream in, OutputStream out, OutputStream os,
            String enc, Socket socket, JTextArea textArea, boolean hex,
            String tcpDumpFilename) {

        this.in = in;
        this.out = out;
        this.os = os;
        this.enc = (enc == null ? "8859_1" : enc);
        this.socket = socket;
        this.textArea = textArea;
        this.hex = hex;

        if (tcpDumpFilename != null && tcpDumpFilename.trim().length() > 0) {
            try {
                this.fos = new FileOutputStream(tcpDumpFilename);
            } catch (Exception ex) {
                System.out.println("Cannot write TCP request to file : " + tcpDumpFilename);
            }
        } else {
            fos = null;
        }
    }

    public void run() {
        int n;
        try {
            while (!exit && (n = in.read(buf)) > 0) {

                if (sb.length() > 12 * 1024) {
                    sb.delete(0, 4 * 1024);
                }

                if (hex) {
                    sb.append(HexDump.dumpHexData(buf, 0, n));
                } else {
                    sb.append(new String(buf, 0, n));
                }
                textArea.setText(sb.toString());

                out.write(buf, 0, n);
                out.flush();

                if (os != null) {
                    os.write(buf, 0, n);
                    os.flush();
                }

                if (fos != null) {
                    fos.write(buf, 0, n);
                    fos.flush();
                }
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
            }
        }
    }

    public void setExit(boolean exit) {
        this.exit = exit;
    }
}
