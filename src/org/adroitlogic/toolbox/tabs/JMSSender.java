/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.tabs;

import java.util.Date;
import javax.jms.*;
import javax.naming.InitialContext;
import java.util.Hashtable;

/**
 *
 * @author asankha
 */
public class JMSSender {

    public static String sendMessage(
            String destination, String connFac, String jndiPropString,
            String correlationId, String messageId, String replyTo,
            String expiration, boolean persistent, String priority,
            boolean redelivered, String timestamp, boolean text,
            String messageProperties,
            String payload) {

        try {
            Hashtable props = new Hashtable();
            if (!jndiPropString.endsWith("\n")) {
                jndiPropString += "\n";
            }
            String[] lines = jndiPropString.split("\n");
            for (int i = 0; i < lines.length; i++) {
                final int pos = lines[i].indexOf('=');
                if (pos != -1) {
                    props.put(lines[i].substring(0, pos), lines[i].substring(pos + 1));
                } else {
                    return "JNDI properties must be specified as: " +
                            "\n<propertyName1>=<propertyValue1>" +
                            "\n<propertyName2>=<propertyValue2>\n\nPlease correct: \n" + jndiPropString;
                }
            }
            InitialContext iniCtx = new InitialContext(props);
            ConnectionFactory connectionFactory = (ConnectionFactory) iniCtx.lookup(connFac);
            Connection connection = connectionFactory.createConnection();

            Destination dest = (Destination) iniCtx.lookup(destination);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();

            MessageProducer sender = session.createProducer(dest);
            Message msg = null;
            if (text) {
                msg = session.createTextMessage(payload);
            } else {
                msg = session.createBytesMessage();
                ((BytesMessage) msg).writeBytes(payload.getBytes());
            }

            if (correlationId != null && correlationId.length() > 0) {
                msg.setJMSCorrelationID(correlationId);
            }
            if (messageId != null && messageId.length() > 0) {
                msg.setJMSMessageID(messageId);
            }
            if (replyTo != null && replyTo.length() > 0) {
                msg.setJMSReplyTo((Destination) iniCtx.lookup(replyTo));
            }
            if (expiration != null && expiration.length() > 0) {
                msg.setJMSExpiration(Long.parseLong(expiration));
            }
            if (persistent) {
                msg.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
            }
            if (priority != null && priority.length() > 0) {
                msg.setJMSPriority(Integer.parseInt(priority));
            }
            if (redelivered) {
                msg.setJMSRedelivered(true);
            }
            if (timestamp != null && timestamp.length() > 0) {
                msg.setJMSTimestamp(Long.parseLong(timestamp));
            }

            if (messageProperties != null && messageProperties.length() > 0) {
                if (!messageProperties.endsWith("\n")) {
                    messageProperties += "\n";
                }
                lines = messageProperties.split("\n");
                for (int i = 0; i < lines.length; i++) {
                    final String[] parts = lines[i].split(":");
                    if (parts.length == 3) {
                        if ("int".equals(parts[0])) {
                            msg.setIntProperty(parts[1], Integer.parseInt(parts[2]));
                        }
                        if ("long".equals(parts[0])) {
                            msg.setLongProperty(parts[1], Long.parseLong(parts[2]));
                        }
                        if ("boolean".equals(parts[0])) {
                            msg.setBooleanProperty(parts[1], Boolean.parseBoolean(parts[2]));
                        }
                        if ("String".equals(parts[0])) {
                            msg.setStringProperty(parts[1], parts[2]);
                        }
                    } else {
                        return "JMS Message properties must be specified as: "
                            + "\n<type>:<propertyName1>:<propertyValue1>"
                            + "\n<type>:<propertyName2>:<propertyValue2>"
                            + "\nWhere type is int, long, boolean or String"
                            + "\n\nPlease correct: \n" + messageProperties;
                    }
                }
            }

            sender.send(msg);

            sender.close();
            connection.stop();
            session.close();
            connection.close();
            return "Message sent at : " + new Date();

        } catch (Exception e) {
            return "Error sending JMS Message : " + e.getMessage();
        }
    }
}
