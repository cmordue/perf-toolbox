/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adroitlogic.toolbox.tabs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 *
 * @author asankha
 */
public class SocketClient {

    public String send(String host, int port, String text) {

        try {
            InetAddress addr = InetAddress.getByName(host);
            Socket socket = new Socket(addr, port);

            text = text.replaceAll("\n", "\r\n");
            BufferedWriter wr = new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
            wr.write(text);
            wr.flush();

            // Get response
            BufferedReader rd = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
            StringBuilder sb = new StringBuilder(1024);
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line).append("\r\n");
            }
            wr.close();
            rd.close();

            return sb.toString();

        } catch (Exception e) {
            return "Encountered an ERROR : " + e.getMessage();
        }
    }
}
