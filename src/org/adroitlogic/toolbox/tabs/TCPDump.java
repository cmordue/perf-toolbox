/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adroitlogic.toolbox.tabs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextArea;

/**
 *
 * @author asankha
 */
public class TCPDump {

    private Thread t;
    private Relay r1;
    private Relay r2;
    private Socket listen;
    private ServerSocket ss;

    public synchronized void start(
            final int listenPort, final String forwardHost, final int forwardPort,
            final String enc, final JTextArea reqArea, final JTextArea resArea,
            final boolean hex, final String tcpDumpFilename) {

        t = new Thread(new Runnable() {

            public void run() {
                try {
                    ss = new ServerSocket(listenPort);

                    while (true) {
                        try {
                            listen = ss.accept();
                        } catch (Exception ignore) {
                            return;
                        }

                        Socket tunnel = new Socket(forwardHost, forwardPort);

                        r1 = new Relay(listen.getInputStream(), tunnel.getOutputStream(),
                                null, enc, listen, reqArea, hex, tcpDumpFilename);
                        r1.start();
                        r2 = new Relay(tunnel.getInputStream(), listen.getOutputStream(),
                                null, enc, listen, resArea, hex, null);
                        r2.start();
                    }
                } catch (Exception e) {
                    System.out.println("Error starting TCP Dump on port : " +
                            listenPort + " - " + e.getMessage());
                } finally {
                    if (listen != null) {
                        try {
                            listen.close();
                        } catch (IOException ignore) {
                        }
                    }
                    if (ss != null) {
                        try {
                            ss.close();
                        } catch (IOException ignore) {
                        }
                    }
                }
            }
        });
        t.start();
    }

    public void stop() {
        if (ss != null) {
            try {
                ss.close();
            } catch (IOException ignore) {}
        }
        if (r1 != null) {
            r1.setExit(true);
            r1.interrupt();
        }
        if (r2 != null) {
            r2.setExit(true);
            r2.interrupt();
        }
    }
}
