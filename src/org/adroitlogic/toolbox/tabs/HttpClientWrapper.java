/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adroitlogic.toolbox.tabs;

import org.adroitlogic.toolbox.ext.NTLMSchemeFactory;
import org.adroitlogic.toolbox.ext.TBHttpClient;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author asankha
 */
public class HttpClientWrapper {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientWrapper.class);

    private final TrustManager easyTrustManager;
    private final DefaultHttpClient client;

    public HttpClientWrapper() {

        easyTrustManager = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain,
                String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain,
                String authType) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };

        client =  new TBHttpClient();
        client.getAuthSchemes().register("ntlm", new NTLMSchemeFactory());

        client.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
                "AdroitLogic (http://adroitlogic.org) - SOA Toolbox/1.5.0");
        client.addResponseInterceptor(new HttpResponseInterceptor() {

            public void process(
                    final HttpResponse response,
                    final HttpContext context) throws HttpException, IOException {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    Header ceheader = entity.getContentEncoding();
                    if (ceheader != null) {
                        HeaderElement[] codecs = ceheader.getElements();
                        for (int i = 0; i < codecs.length; i++) {
                            if (codecs[i].getName().equalsIgnoreCase("gzip")) {
                                response.setEntity(
                                        new GzipDecompressingEntity(response.getEntity()));
                                return;
                            }
                        }
                    }
                }
            }
        });
    }

    public String execute(
            String url, String method, String contentType,
            String soapAction,
            String headers,
            boolean disableSSLChecks,
            String trustStorePath,
            String trustStorePassword,
            String identityStorePath,
            String identityStorePassword,
            String encoding,
            String socketTimeout,
            boolean useHttp10, 
            boolean useChunked,
            boolean useExpectContinue,
            boolean useKeepalive, 
            boolean acceptGzip,
            String fileName,
            String requestBody,
            boolean useBasicAuth,
            boolean useDigestAuth,
            boolean useNtlmAuth,
            String username,
            String password,
            String realm,
            String domain,
            String workstation,
            boolean debugMessages,
            boolean debugSSL) {

        if (debugSSL) {
            System.setProperty("javax.net.debug", "all");
        } else {
            System.clearProperty("javax.net.debug");
        }

        if (url.startsWith("https")) {
            try {
                int port = 443;
                final URL u = new URL(url);
                if (u.getPort() != -1) {
                    port = u.getPort();
                }

                KeyStore identityStore = null;
                if (identityStorePath != null && identityStorePath.length() > 0) {
                    identityStore = KeyStore.getInstance(KeyStore.getDefaultType());
                    FileInputStream instream = new FileInputStream(identityStorePath);
                    try {
                        identityStore.load(instream, identityStorePassword.toCharArray());
                    } finally {
                        instream.close();
                    }
                }

                if (disableSSLChecks) {
                    SSLContext sslcontext = SSLContext.getInstance("TLS");
                    if (identityStore != null) {
                        KeyManagerFactory kmf = KeyManagerFactory.getInstance(
                            KeyManagerFactory.getDefaultAlgorithm());
                        kmf.init(identityStore, identityStorePassword.toCharArray());
                        sslcontext.init(kmf.getKeyManagers(), new TrustManager[]{easyTrustManager}, null);
                    } else {
                        sslcontext.init(null, new TrustManager[]{easyTrustManager}, null);
                    }

                    SSLSocketFactory sf = new SSLSocketFactory(sslcontext);
                    sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                    Scheme sch = new Scheme("https", sf, port);
                    client.getConnectionManager().getSchemeRegistry().register(sch);

                } else {
                    KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                    FileInputStream instream = new FileInputStream(trustStorePath);
                    try {
                        trustStore.load(instream, trustStorePassword.toCharArray());
                        SSLSocketFactory socketFactory = null;
                        if (identityStore != null) {
                            socketFactory = new SSLSocketFactory(identityStore, identityStorePassword, trustStore);
                        } else {
                            socketFactory = new SSLSocketFactory(trustStore);
                        }
                        Scheme sch = new Scheme("https", socketFactory, port);
                        client.getConnectionManager().getSchemeRegistry().register(sch);
                    } finally {
                        instream.close();
                    }
                }
            } catch (Exception e) {
                logger.error("Error initializing SSL : " + e.getMessage(), e);
            }
        }

        if (debugMessages) {
            org.apache.log4j.Logger.getLogger("org.adroitlogic.toolbox.ext.wire").setLevel(Level.DEBUG);
        } else {
            org.apache.log4j.Logger.getLogger("org.adroitlogic.toolbox.ext.wire").setLevel(Level.INFO);
        }

        client.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
                useHttp10 ? HttpVersion.HTTP_1_0 : HttpVersion.HTTP_1_1);
        client.getParams().setParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE,
                useExpectContinue ? Boolean.TRUE : Boolean.FALSE);
        client.getParams().setParameter("http.socket.timeout", Integer.parseInt(socketTimeout) * 1000);
        if (useKeepalive) {
            client.setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {

                public long getKeepAliveDuration(HttpResponse arg0, HttpContext arg1) {
                    return 60000;
                }
            });
        } else {
            client.setKeepAliveStrategy(null);
        }

        BasicHttpContext localContext = null;
        if (useBasicAuth) {
            HttpHost targetHost = null;
            try {
                URL reqUrl = new URL(url);
                targetHost = new HttpHost(
                    reqUrl.getHost(),
                    (reqUrl.getPort() == -1 ? reqUrl.getDefaultPort() : reqUrl.getPort()),
                    url.startsWith("https") ? "https" : "http");
            } catch (MalformedURLException e) {
                return "Invalid URL : " + url + " - " + e.getMessage();
            }

            client.getCredentialsProvider().setCredentials(
                AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

            // Add AuthCache to the execution context
            localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.AUTH_CACHE, authCache);

        } else if (useDigestAuth) {

            HttpHost targetHost = null;
            try {
                URL reqUrl = new URL(url);
                targetHost = new HttpHost(
                        reqUrl.getHost(),
                        (reqUrl.getPort() == -1 ? reqUrl.getDefaultPort() : reqUrl.getPort()),
                        url.startsWith("https") ? "https" : "http");
            } catch (MalformedURLException e) {
                return "Invalid URL : " + url + " - " + e.getMessage();
            }

            client.getCredentialsProvider().setCredentials(
                AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));
                
//            // Create AuthCache instance
//            AuthCache authCache = new BasicAuthCache();
//            // Generate DIGEST scheme object, initialize it and add it to the local
//            // auth cache
//            DigestScheme digestAuth = new DigestScheme();
//            // Suppose we already know the realm name
//            digestAuth.overrideParamter("realm", realm);
//            // Suppose we already know the expected nonce value
//            digestAuth.overrideParamter("nonce", "adroitlogic");
//            authCache.put(targetHost, digestAuth);
//
//            // Add AuthCache to the execution context
//            localContext = new BasicHttpContext();
//            localContext.setAttribute(ClientContext.AUTH_CACHE, authCache);

        } else if (useNtlmAuth) {
            client.getCredentialsProvider().setCredentials(
                AuthScope.ANY,
                new NTCredentials(username, password, workstation, domain));
        }


        try {
            if ("POST".equals(method)) {
                HttpPost httpPost = new HttpPost(url);
                if (fileName == null) {
                    StringEntity entity = new StringEntity(requestBody, contentType, encoding);
                    entity.setChunked(useChunked);
                    httpPost.setEntity(entity);
                } else {
                    FileEntity entity = new FileEntity(new File(fileName), contentType);
                    entity.setChunked(useChunked);
                    httpPost.setEntity(entity);
                }
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpPost, localContext);

            } else if ("GET".equals(method)) {
                HttpGet httpGet = new HttpGet(url);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpGet, localContext);

            } else if ("PUT".equals(method)) {
                HttpPut httpPut = new HttpPut(url);
                StringEntity entity = new StringEntity(requestBody, contentType, encoding);
                entity.setChunked(useChunked);
                httpPut.setEntity(entity);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpPut, localContext);

            } else if ("DELETE".equals(method)) {
                HttpDelete httpDelete = new HttpDelete(url);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpDelete, localContext);

            } else if ("HEAD".equals(method)) {
                HttpHead httpHead = new HttpHead(url);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpHead, localContext);

            } else if ("OPTIONS".equals(method)) {
                HttpOptions httpOptions = new HttpOptions(url);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpOptions, localContext);

            } else if ("TRACE".equals(method)) {
                HttpTrace httpTrace = new HttpTrace(url);
                return returnResponse(useKeepalive, useExpectContinue, acceptGzip, soapAction, headers, httpTrace, localContext);
            }
        } catch (Exception e) {
            logger.error("Error executing HTTP client", e);
            return "=== Encountered an ERROR : " + e.getMessage();
        }
        return null;
    }

    private String returnResponse(boolean useKeepalive, boolean useExpectContinue, boolean acceptGZip,
            String soapAction, String headers, HttpUriRequest httpReq, BasicHttpContext localContext) {

        StringBuilder sb = new StringBuilder(1024);
        try {
            if (!useKeepalive) {
                httpReq.addHeader("Connection", "close");
            }
            if (soapAction != null) {
                httpReq.addHeader("SOAPAction", soapAction);
            }
            if (acceptGZip) {
                httpReq.addHeader("Accept-Encoding", "gzip");
            }
            if (headers.length() > 0) {
                if (!headers.endsWith("\n")) {
                    headers += "\n";
                }
                String[] header = headers.split("\n");
                for (int i=0; i<header.length; i++) {
                    final int pos = header[i].indexOf(": ");
                    if (pos != -1) {
                        httpReq.addHeader(header[i].substring(0, pos), header[i].substring(pos+2));
                    } else {
                        return "Headers must be specified as: " +
                                "\n<headerName1>: <headerValue1>"+
                                "\n<headerName2>: <headerValue2>\n\nPlease correct: \n" + headers;
                    }
                }
            }

            HttpResponse response = client.execute(httpReq, localContext);
            sb.append(response.getStatusLine().toString()).append("\r\n");

            for (Header h : response.getAllHeaders()) {
                sb.append(h.getName()).append(": ").append(h.getValue()).append("\r\n");
            }
            sb.append("\r\n");

            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                response.getEntity().writeTo(baos);
                sb.append(new String(baos.toByteArray()));
            } catch (Exception ignore) {
            }
            return sb.toString();

        } catch (Exception e) {
            if (sb != null) {
                logger.error("Error processing response", e);
                sb.append("=== Encountered ERROR : " + e.getMessage() + " ===");
                return sb.toString();
            }
        }
        return null;
    }

    static class GzipDecompressingEntity extends HttpEntityWrapper {

        public GzipDecompressingEntity(final HttpEntity entity) {
            super(entity);
        }

        @Override
        public InputStream getContent()
                throws IOException, IllegalStateException {

            // the wrapped entity's getContent() decides about repeatability
            InputStream wrappedin = wrappedEntity.getContent();

            return new GZIPInputStream(wrappedin);
        }

        @Override
        public long getContentLength() {
            // length of ungzipped content is not known
            return -1;
        }
    }
}
