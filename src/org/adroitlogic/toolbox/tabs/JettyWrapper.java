/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.adroitlogic.toolbox.tabs;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.bio.SocketConnector;
import org.mortbay.jetty.webapp.WebAppContext;
import org.mortbay.thread.QueuedThreadPool;

/**
 *
 * @author asankha
 */
public class JettyWrapper {

    private static Server server = null;

    public void start(int port) {
        try {
            LogManager.getLogManager().getLogger("").setLevel(Level.OFF);
            server = new Server(port);
            server.setThreadPool(new QueuedThreadPool(256));
            SocketConnector sc = new SocketConnector();
            sc.setMaxIdleTime(120000);
            server.addConnector(sc);

//            Context context1 = new Context(server, "/hessian", Context.SESSIONS);
//            context1.addServlet(HessianStockQuoteService.class, "/hessian-stockquote");
//            server.addHandler(context1);
//
//            Context context2 = new Context(server, "/hessiantest", Context.SESSIONS);
//            context2.addServlet(ArrayMultiplicationHessian.class, "/hessian-stress-test");
//            server.addHandler(context2);

            WebAppContext webAppContext = new WebAppContext("samples/webapp", "/");
            webAppContext.setConfigurationClasses(
                    new String[]{
                        "org.mortbay.jetty.webapp.WebInfConfiguration",
                        "org.mortbay.jetty.webapp.WebXmlConfiguration"});
            server.addHandler(webAppContext);

            server.start();
        } catch (Exception ex) {
            Logger.getLogger(JettyWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stop() {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception ex) {
                Logger.getLogger(JettyWrapper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
