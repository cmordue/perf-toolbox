/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.javabench;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author asankha
 */
public class JavaBench {
    
    public static void main(String[] args) throws Exception {

        PropertyConfigurator.configure(ClassLoader.getSystemClassLoader().
            getResource("org/adroitlogic/toolbox/resources/log4j.properties"));

        Options options = CommandLineUtils.getOptions();
        CommandLineParser parser = new PosixParser();
        CommandLine cmd = parser.parse(options, args);

        if (args.length == 0 || cmd.hasOption('h') || cmd.getArgs().length != 1) {
            CommandLineUtils.showUsage(options);
            System.exit(1);
        }

        HttpBenchmark httpBenchmark = new HttpBenchmark();
        CommandLineUtils.parseCommandLine(cmd, httpBenchmark);
        httpBenchmark.execute();
    }
}
