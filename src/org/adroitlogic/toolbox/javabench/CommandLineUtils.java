/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.javabench;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class CommandLineUtils {

    static Options getOptions() {

        Option oopt = new Option("o", false, "Use HTTP/S 1.0");
        oopt.setRequired(false);

        Option kopt = new Option("k", false, "Enable the HTTP KeepAlive feature, " +
                "i.e., perform multiple requests within one HTTP session. " +
                "Default is no KeepAlive");
        kopt.setRequired(false);

        Option uopt = new Option("u", false, "Chunk entity. Default is false");
        uopt.setRequired(false);

        Option xopt = new Option("x", false, "Use Expect-Continue. Default is false");
        xopt.setRequired(false);

        Option gopt = new Option("g", false, "Accept GZip. Default is false");
        gopt.setRequired(false);

        Option nopt = new Option("n", true, "Number of requests to perform for the " +
                "benchmarking session. The default is to just perform a single " +
                "request which usually leads to non-representative benchmarking " +
                "results.");
        nopt.setRequired(false);
        nopt.setArgName("requests");

        Option copt = new Option("c", true, "Concurrency while performing the " +
                "benchmarking session. The default is to just use a single thread/client.");
        copt.setRequired(false);
        copt.setArgName("concurrency");

        Option popt = new Option("p", true, "File containing data to POST.");
        popt.setRequired(false);
        popt.setArgName("POST-postFile");

        Option mopt = new Option("m", true, "HTTP Method. Default is POST. " +
                "Possible options are GET, POST, PUT, DELETE, HEAD, OPTIONS, TRACE");
        mopt.setRequired(false);
        mopt.setArgName("HTTP method");

        Option Topt = new Option("T", true, "Content-type header to use for POST data.");
        Topt.setRequired(false);
        Topt.setArgName("content-type");

        Option topt = new Option("t", true, "Client side socket timeout (in ms) - default 60 Secs");
        topt.setRequired(false);
        topt.setArgName("socket-Timeout");

        Option Hopt = new Option("H", true, "Add arbitrary header line, " +
                "eg. 'Accept-Encoding: gzip' inserted after all normal " +
                "header lines. (repeatable as -H \"h1: v1\",\"h2: v2\" etc)");
        Hopt.setRequired(false);
        Hopt.setArgName("header");

        Option vopt = new Option("v", true, "Set verbosity level - 4 and above " +
                "prints response content, 3 and above prints " +
                "information on headers, 2 and above prints response codes (404, 200, " +
                "etc.), 1 and above prints warnings and info.");
        vopt.setRequired(false);
        vopt.setArgName("verbosity");

        Option hopt = new Option("h", false, "Display usage information.");
        nopt.setRequired(false);

        Options options = new Options();
        options.addOption(mopt);
        options.addOption(uopt);
        options.addOption(xopt);
        options.addOption(gopt);
        options.addOption(kopt);
        options.addOption(nopt);
        options.addOption(copt);
        options.addOption(popt);
        options.addOption(Topt);
        options.addOption(vopt);
        options.addOption(Hopt);
        options.addOption(hopt);
        options.addOption(topt);
        options.addOption(oopt);
        return options;
    }

    static void parseCommandLine(CommandLine cmd, HttpBenchmark httpBenchmark) {

        if (cmd.hasOption('v')) {
            String s = cmd.getOptionValue('v');
            try {
                httpBenchmark.verbosity = Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                printError("Invalid verbosity level: " + s);
            }
        }

        if (cmd.hasOption('k')) {
            httpBenchmark.keepAlive = true;
        }

        if (cmd.hasOption('c')) {
            String s = cmd.getOptionValue('c');
            try {
                httpBenchmark.threads = Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                printError("Invalid number for concurrency: " + s);
            }
        }

        if (cmd.hasOption('n')) {
            String s = cmd.getOptionValue('n');
            try {
                httpBenchmark.requests = Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                printError("Invalid number of requests: " + s);
            }
        }

        try {
            httpBenchmark.url = new URL(cmd.getArgs()[0]);
        } catch (MalformedURLException e) {
            printError("Invalid request URL : " + cmd.getArgs()[0]);
        }

        if (cmd.hasOption('p')) {
            httpBenchmark.payloadFile = cmd.getOptionValue('p');
            if (!new File(httpBenchmark.payloadFile).exists()) {
                printError("File not found: " + httpBenchmark.payloadFile);
            }
        }

        if (cmd.hasOption('T')) {
            httpBenchmark.contentType = cmd.getOptionValue('T');
        }

        if (cmd.hasOption('H')) {
            String headerStr = cmd.getOptionValue('H');
            LinkedList<String> headers = new LinkedList<String>();
            for (String a : headerStr.split(",")) {
                if (a.indexOf(':') != -1) {
                    headers.add(a);
                } else {
                    headers.add(headers.removeLast() + "," + a);
                }
            }
            String[] headerArray = new String[headers.size()];
            httpBenchmark.headers = headers.toArray(headerArray);
        }

        if (cmd.hasOption('t')) {
            String t = cmd.getOptionValue('t');
            try {
                httpBenchmark.socketTimeout = Integer.parseInt(t);
            } catch (NumberFormatException ex) {
                printError("Invalid socket timeout: " + t);
            }
        }

        if (cmd.hasOption('o')) {
            httpBenchmark.useHttp1_0 = true;
        }

        if (cmd.hasOption('m')) {
            httpBenchmark.method = cmd.getOptionValue('m');
        } else {
            httpBenchmark.method = "POST";
        }

        if (cmd.hasOption('u')) {
            httpBenchmark.useChunking = true;
        }

        if (cmd.hasOption('x')) {
            httpBenchmark.useExpectContinue = true;
        }

        if (cmd.hasOption('g')) {
            httpBenchmark.useAcceptGZip = true;
        }
    }

    static void showUsage(final Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("SOA-Toolbox/1.8.0 (c) 2010 AdroitLogic. All Rights Reserved" +
                "\nJavaBench [options] http[s]://hostname[:port]/path", options);
    }

    static void printError(String msg) {
        System.err.println(msg);
        showUsage(getOptions());
        System.exit(-1);
    }
}
