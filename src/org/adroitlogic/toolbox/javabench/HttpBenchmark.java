/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */
package org.adroitlogic.toolbox.javabench;

import org.apache.http.*;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.File;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author asankha
 */
public class HttpBenchmark {

    private HttpParams params = null;
    private HttpRequest[] request = null;
    private HttpHost host = null;
    
    protected String method = null;
    protected URL url = null;
    protected String contentType = null;
    protected int verbosity = 0;
    protected int requests = 1;
    protected int threads = 1;
    private long contentLength = -1;
    protected String[] headers = null;

    protected int socketTimeout = 60000;
    protected String payloadFile = null;
    protected String payloadText = null;
    protected String soapAction = null;
    protected String trustStorePath = null;
    protected String trustStorePassword = null;

    protected boolean useHttp1_0 = false;
    protected boolean useChunking = false;
    protected boolean useExpectContinue = false;
    protected boolean keepAlive = false;
    protected boolean useAcceptGZip = false;
    protected boolean disableSSLVerification = true;

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }BenchmarkWorker[] workers = new BenchmarkWorker[threads];

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setDisableSSLVerification(boolean disableSSLVerification) {
        this.disableSSLVerification = disableSSLVerification;
    }

    public void setKeepAlive(boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setPayloadFile(String payloadFile) {
        this.payloadFile = payloadFile;
    }

    public void setPayloadText(String payloadText) {
        this.payloadText = payloadText;
    }

    public void setRequests(int requests) {
        this.requests = requests;
    }

    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

    public void setSocketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public void setTrustStorePassword(String trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }

    public void setTrustStorePath(String trustStorePath) {
        this.trustStorePath = trustStorePath;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public void setUseAcceptGZip(boolean useAcceptGZip) {
        this.useAcceptGZip = useAcceptGZip;
    }

    public void setUseChunking(boolean useChunking) {
        this.useChunking = useChunking;
    }

    public void setUseExpectContinue(boolean useExpectContinue) {
        this.useExpectContinue = useExpectContinue;
    }

    public void setUseHttp1_0(boolean useHttp1_0) {
        this.useHttp1_0 = useHttp1_0;
    }

    public void setVerbosity(int verbosity) {
        this.verbosity = verbosity;
    }


    
    private void prepare() throws Exception {
        // prepare http params
        params = getHttpParams(socketTimeout, useHttp1_0, useExpectContinue);
        host = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());

        HttpEntity entity = null;
        if (payloadFile != null) {
            File file = new File(payloadFile);
            entity = new FileEntity(file, contentType);
            ((FileEntity) entity).setChunked(useChunking);
            contentLength = file.length();

        } else if (payloadText.length() > 0) {
            entity = new StringEntity(payloadText, contentType, "UTF-8");
            ((StringEntity) entity).setChunked(useChunking);
            contentLength = payloadText.getBytes().length;
        }

        // Prepare requests for each thread
        request = new HttpRequest[threads];

        for (int i = 0; i < threads; i++) {
            if ("POST".equals(method)) {
                BasicHttpEntityEnclosingRequest httppost =
                        new BasicHttpEntityEnclosingRequest("POST", url.getPath());
                httppost.setEntity(entity);
                request[i] = httppost;
            } else if ("PUT".equals(method)) {
                BasicHttpEntityEnclosingRequest httpput =
                        new BasicHttpEntityEnclosingRequest("PUT", url.getPath());
                httpput.setEntity(entity);
                request[i] = httpput;
            } else {
                String path = url.getPath();
                if (url.getQuery() != null) {
                    path += "?" + url.getQuery();
                }
                request[i] = new BasicHttpRequest(method, path);
            }
        }

        if (!keepAlive) {
            for (int i = 0; i < threads; i++) {
                request[i].addHeader(new DefaultHeader("Connection", "close"));
            }
        }

        if (headers != null) {
            for (int i = 0; i < headers.length; i++) {
                String s = headers[i];
                int pos = s.indexOf(": ");
                if (pos != -1) {
                    Header header = new DefaultHeader(
                            s.substring(0, pos).trim(), s.substring(pos + 1).trim());
                    for (int j = 0; j < threads; j++) {
                        request[j].addHeader(header);
                    }
                }
            }
        }

        if (useAcceptGZip) {
            for (int i = 0; i < threads; i++) {
                request[i].addHeader(new DefaultHeader("Accept-Encoding", "gzip"));
            }
        }

        if (soapAction != null && soapAction.length() > 0) {
            for (int i = 0; i < threads; i++) {
                request[i].addHeader(new DefaultHeader("SOAPAction", soapAction));
            }
        }
    }

    public String execute() throws Exception {
        

        prepare();

        ThreadPoolExecutor workerPool = new ThreadPoolExecutor(
            threads, threads, 5, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>(),
            new ThreadFactory() {
                public Thread newThread(Runnable r) {
                    return new Thread(r, "ClientPool");
                }
                
            });
        workerPool.prestartAllCoreThreads();

        BenchmarkWorker[] workers = new BenchmarkWorker[threads];
        for (int i = 0; i < threads; i++) {
            workers[i] = new BenchmarkWorker(
                    params,
                    verbosity, 
                    request[i], 
                    host, 
                    requests, 
                    keepAlive,
                    disableSSLVerification,
                    trustStorePath,
                    trustStorePassword);
            workerPool.execute(workers[i]);
        }

        while (workerPool.getCompletedTaskCount() < threads) {
            Thread.yield();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignore) {}
        }

        workerPool.shutdown();
        return ResultProcessor.printResults(workers, host, url.toString(), contentLength);
    }

    private HttpParams getHttpParams(
            int socketTimeout, boolean useHttp1_0, boolean useExpectContinue) {

        HttpParams params = new BasicHttpParams();
        params.setParameter(HttpProtocolParams.PROTOCOL_VERSION,
            useHttp1_0 ? HttpVersion.HTTP_1_0 : HttpVersion.HTTP_1_1)
            .setParameter(HttpProtocolParams.USER_AGENT, "SOA-Toolbox/1.8.0")
            .setBooleanParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, useExpectContinue)
            .setBooleanParameter(HttpConnectionParams.STALE_CONNECTION_CHECK, false)
            .setIntParameter(HttpConnectionParams.SO_TIMEOUT, socketTimeout);
        return params;
    }
}
