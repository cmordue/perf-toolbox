/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JettyPanel.java
 *
 * Created on Dec 10, 2009, 4:32:06 PM
 */

package org.adroitlogic.toolbox;

import java.util.HashMap;
import java.util.Map;
import org.adroitlogic.toolbox.tabs.JettyWrapper;

/**
 *
 * @author asankha
 */
public class JettyPanel extends javax.swing.JPanel implements StateManagement {

    private JettyWrapper jetty = new JettyWrapper();

    /** Creates new form JettyPanel */
    public JettyPanel() {
        initComponents();
    }

    public void shutdown() {
        jetty.stop();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel18 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jettyStop = new javax.swing.JButton();
        jettyStart = new javax.swing.JButton();
        jettyPort = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        delaySpinner = new javax.swing.JSpinner();

        setMaximumSize(new java.awt.Dimension(1280, 760));
        setName("Form"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(org.adroitlogic.toolbox.ToolBoxApp.class).getContext().getResourceMap(JettyPanel.class);
        jLabel18.setText(resourceMap.getString("jLabel18.text")); // NOI18N
        jLabel18.setName("jLabel18"); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jPanel1.border.title"))); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jEditorPane1.setContentType("text/html"); // NOI18N
        jEditorPane1.setText(resourceMap.getString("jEditorPane1.text")); // NOI18N
        jEditorPane1.setName("jEditorPane1"); // NOI18N
        jScrollPane1.setViewportView(jEditorPane1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jettyStop.setText(resourceMap.getString("jettyStop.text")); // NOI18N
        jettyStop.setEnabled(false);
        jettyStop.setName("jettyStop"); // NOI18N
        jettyStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jettyStopActionPerformed(evt);
            }
        });

        jettyStart.setText(resourceMap.getString("jettyStart.text")); // NOI18N
        jettyStart.setName("jettyStart"); // NOI18N
        jettyStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jettyStartActionPerformed(evt);
            }
        });

        jettyPort.setText(resourceMap.getString("jettyPort.text")); // NOI18N
        jettyPort.setName("jettyPort"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        delaySpinner.setName("delaySpinner"); // NOI18N
        delaySpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                delaySpinnerStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(26, 26, 26)
                        .addComponent(jettyPort, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jettyStart)
                        .addGap(18, 18, 18)
                        .addComponent(jettyStop)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(delaySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jettyPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jettyStart)
                    .addComponent(jettyStop)
                    .addComponent(delaySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(29, 29, 29)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jettyStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jettyStartActionPerformed
        jetty.start(Integer.parseInt(jettyPort.getText()));
        jettyStart.setEnabled(false);
        jettyStop.setEnabled(true);
}//GEN-LAST:event_jettyStartActionPerformed

    private void jettyStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jettyStopActionPerformed
        jetty.stop();
        jettyStart.setEnabled(true);
        jettyStop.setEnabled(false);
}//GEN-LAST:event_jettyStopActionPerformed

    private void delaySpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_delaySpinnerStateChanged
//        EchoService.delayMillis = Long.valueOf((Integer) delaySpinner.getValue());
    }//GEN-LAST:event_delaySpinnerStateChanged

    @Override
    public Map save() {
        Map map = new HashMap();
        map.put("class", getClass().getSimpleName());
        map.put("jettyPort", jettyPort.getText());

        return map;
    }

    @Override
    public void load(Map map) {

        jettyPort.setText((String) map.get("jettyPort"));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner delaySpinner;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jettyPort;
    private javax.swing.JButton jettyStart;
    private javax.swing.JButton jettyStop;
    // End of variables declaration//GEN-END:variables

}
