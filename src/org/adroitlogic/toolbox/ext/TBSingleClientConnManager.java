/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.ext;

import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.HttpParams;

/**
 *
 * @author asankha
 */
public class TBSingleClientConnManager extends SingleClientConnManager {

    public TBSingleClientConnManager(SchemeRegistry schreg) {
        super(schreg);
    }

    public TBSingleClientConnManager(HttpParams params, SchemeRegistry schreg) {
        super(params, schreg);
    }

    @Override
    protected ClientConnectionOperator createConnectionOperator(SchemeRegistry schreg) {
        return new TBClientConnectionOperator(schreg);
    }

}
