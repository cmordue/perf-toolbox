/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.adroitlogic.toolbox.ext;

import org.apache.http.impl.auth.NTLMEngine;
import org.apache.http.impl.auth.NTLMEngineException;

/**
 * @author asankha
 */
import jcifs.ntlmssp.Type1Message;
import jcifs.ntlmssp.Type2Message;
import jcifs.ntlmssp.Type3Message;
import jcifs.util.Base64;

import java.io.IOException;

/**
 * Copied from Apache HttpComponents/HttpClient
 */
public class JCIFSEngine implements NTLMEngine {

    @Override
    public String generateType1Msg(
            String domain,
            String workstation) throws NTLMEngineException {

        Type1Message t1m = new Type1Message(
                Type1Message.getDefaultFlags(),
                domain,
                workstation);
        return Base64.encode(t1m.toByteArray());
    }

    @Override
    public String generateType3Msg(
            String username,
            String password,
            String domain,
            String workstation,
            String challenge) throws NTLMEngineException {
        Type2Message t2m;
        try {
            t2m = new Type2Message(Base64.decode(challenge));
        } catch (IOException ex) {
            throw new NTLMEngineException("Invalid Type2 message", ex);
        }
        Type3Message t3m = new Type3Message(
                t2m,
                password,
                domain,
                username,
                workstation,
                0);
        return Base64.encode(t3m.toByteArray());
    }
}
