/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.ext;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

/**
 *
 * @author asankha
 */
public class TBHttpClient extends DefaultHttpClient {

    @Override
    protected ClientConnectionManager createClientConnectionManager() {
        ClientConnectionManager ccm = super.createClientConnectionManager();
        if (ccm instanceof SingleClientConnManager) {
            ccm = new TBSingleClientConnManager(ccm.getSchemeRegistry());
        }
        return ccm;
    }

}
