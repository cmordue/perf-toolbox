/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.adroitlogic.toolbox.ext;

import org.apache.http.conn.OperatedClientConnection;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.DefaultClientConnectionOperator;

/**
 *
 * @author asankha
 */
public class TBClientConnectionOperator extends DefaultClientConnectionOperator {

    public TBClientConnectionOperator(SchemeRegistry schemes) {
        super(schemes);
    }

    @Override
    public OperatedClientConnection createConnection() {
        return new TBClientConnection();
    }

}
